﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ConsoleApp1;

public class MyProgram : MonoBehaviour
{
    //[SerializeField]
    //[HideInInspector]
    [Range(1,2)]
    [Tooltip("min 1, max 2")]
    public int shapeSelected;

    // Start is called before the first frame update
    void Start()
    {
        
        Debug.Log("Select shape");
        Debug.Log("1 - Square");
        Debug.Log("2 - Circle");
        Debug.Log(">");

        Shape shape = null;
        switch (shapeSelected)
        {
            case 1:
                shape = new garen("MySquare", 4);
                break;

            case 2:
                shape = new veigar("MyCirlce", 4);
                break;
        }

        if (shape != null)
            shape.Area();

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
