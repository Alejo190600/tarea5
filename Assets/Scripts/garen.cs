﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace ConsoleApp1
{
    class garen : Shape
    {
        public int side;

        public garen(string name, int side) : base(name) {
            this.side = side;
        }

        public override void Area() {
            Debug.Log("Area = " + (side * side));
        }

    }
}
